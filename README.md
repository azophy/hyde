![Build Status](https://gitlab.com/pages/hyde/badges/master/build.svg)

---

Example [hyde] website using GitLab Pages.

Learn more about GitLab Pages at https://pages.gitlab.io and the official
documentation http://doc.gitlab.com/ee/pages/README.html.

---

## GitLab CI

This project's static Pages are built by [GitLab CI][ci], following the steps
defined in [`.gitlab-ci.yml`](.gitlab-ci.yml):

What we need is to run the script on the environment of Python 2.7.x
`image: python:2.7`, within a job called `pages` affecting only the `master`
branch, install Hyde with `pip install hyde` and generate our site to the
`public` folder `hyde gen -d public`. We also add a `vendor` folder for cached
pip modules and dependencies installation for speeding up our builds.

```yaml
image: python:2.7

pages:
  cache:
    paths:
    - vendor/
  script:
  - pip install hyde
  - hyde gen -d public
  artifacts:
    paths:
    - public
  only:
    - master
```

## Building locally

To work locally with this project, you'll have to follow the steps below:

1. Fork, clone or download this project
1. [Install][] Hyde
1. Generate the website: `hyde gen -d public`
1. Preview your project: `hyde serve`
1. Add content

Read more at Hyde's [documentation][].

## GitLab User or Group Pages

To use this project as your user/group website, you will need one additional
step: just rename your project to `namespace.gitlab.io`, where `namespace` is
your `username` or `groupname`. This can be done by navigating to your
project's **Settings**.

Read more about [user/group Pages][userpages] and [project Pages][projpages].

## Did you fork this project?

If you forked this project for your own use, please go to your project's
**Settings** and remove the forking relationship, which won't be necessary
unless you want to contribute back to the upstream project.

## Troubleshooting

1. CSS is missing! That means two things:

    Either that you have wrongly set up the CSS URL in your templates, or
    your static generator has a configuration option that needs to be explicitly
    set in order to serve static assets under a relative URL.

----

Forked from @virtuacreative

[ci]: https://about.gitlab.com/gitlab-ci/
[hyde]: https://hyde.github.io/
[install]: https://hyde.github.io/install.html
[documentation]: https://hyde.github.io/index.html
[userpages]: http://doc.gitlab.com/ee/pages/README.html#user-or-group-pages
[projpages]: http://doc.gitlab.com/ee/pages/README.html#project-pages
